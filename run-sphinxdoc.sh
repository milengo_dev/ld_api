#!/bin/bash

docker run --rm -v ./docs:/docs sphinxdoc/sphinx sphinx-build -M html . _build
