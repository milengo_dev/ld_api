Swift Languages
=====================

Query for supported Swift Languages used when creating automated projects in LanguageDesk.

POST to https://app.languagedesk.com/api/v1/projects/swift_languages

.. csv-table:: **Request**
	:header: "Field","Possible Values","Required?","Comment"
	:widths: 25, 25, 10, 40

	"source_language", "Language code (see list below)", "Yes"
	"target_languages", "List of Language codes (see list below)", "Yes"


.. csv-table:: **Response: List of entries with fields**
	:header: "Field", "Possible values", "Comment"
	:widths: 25, 20, 55

	"source_language", "string", "Source Language code."
	"target_language", "string", "Target Langauge code."
	"swift_ready", "string", "yes or no that indicates if language combination is supported or not"
	"status", "200 or 422", "for success in case of error"
	"messages", "*source_language_not_found
	* target_languages_not_found", Possible error messages

Example requests
++++++++++++++++

**Scenario 1: Query Swift Languages for en->de pair.**

.. code-block:: JSON

	{
		"source_language": "en", 
		"target_languages": ["de-DE"]
	}

**Example response 1 (OK)**

.. code-block:: JSON

	[
		{
			"source_language": "en", 
			"target_language": "de-DE", 
			"swift_ready": true
		}
	]


**Scenario 2: Query Swift Languages for en->de pair.**

.. code-block:: JSON

	{
		"source_language": "the_lang_code", 
		"target_languages": ["de-DE"]
	}

**Example response 2 (ERROR)**

.. code-block:: JSON

	{
		"messages": {
			"source_language_not_found": "source language not found/missing."
		}, 
		"status": 422
	}
	