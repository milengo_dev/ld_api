.. LanguageDesk API documentation master file, created by
   sphinx-quickstart on Mon Aug 31 10:19:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LanguageDesk Requests API's documentation!
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview.rst
   post_project.rst
   post_change_status.rst
   get_project.rst
   get_project_deliver.rst
   download_files.rst
   download_invoices.rst
   authentication.rst
   source_lang.rst
   target_lang.rst
   post_swift_languages.rst
   get_credit_balance.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
