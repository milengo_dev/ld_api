Create Project
=====================

Incoming API project requests are added as new projects in LanguageDesk

POST to https://app.languagedesk.com/api/v1/projects

.. csv-table:: Request
	:header: "Field","Possible Values","Required?","Comment"
	:widths: 25, 25, 10, 40

	"contact_person", "Valid email address of a user registered on Languagedesk.", "Yes", "This user will be made the project owner in LanguageDesk and will be contacted in case of problems or questions."
	"source_language", "Language code (see list below)", "Yes"
	"target_language", "Language code (see list below)", "Yes"
	"content_identifier", "String, max. 20 characters", "Yes", "This identifies the product, content type or source of the translated content."
	"file_transfer_method", "ftp-pull-http-push", "Yes", "* ftp-pull: files are provided on a folder accessible via FTP.
	* http-push: Files are provided along with the request via HTTP multipart/form-data or normal HTTP POST with JSON body (each file content is encoded in base64)."
	"host", "valid host name (without protocol)", "Required only if file_transfer_method is ftp-pull"
	"files->source_path", "path on the FTP server where files to be translated can downloaded.", "Required only if file_transfer_method is ftp-pull"
	"files->target_path", "path on the FTP server where translated files should be pushed to.", "Required only if file_transfer_method is ftp-pull"
	"files->name", "Name of file to be translated", "Yes"
	"files->remove", "Flags the file to be deleted after projects request doen and project created.", "Yes", "True or False"
	"files->md5_checksum", "MD5 checksum of the file", "Required only if file_transfer_method is ftp-pull", "If provided, will perform a check to verify that the file transfer has been completed without errors."
	"directories->{remove:true|false, source_paths: [/import, import2], target_path: /export}", "Source directories that have the source files. Target is where the files are saved back upon delivery", "Required only if file_transfer_method is ftp-pull", "It provides a way to specify source directories to be copied from the FTP server to compress into a source work zip file and the target path to upload deliveries back."
	"mode", "swift", "No", "If not present uses default action and creates new project. If value swift, it will create and process Swift project."
	"instructions", "Allows to fill in project instrucitons", "No", "Allows to fill in project instructions when there are multiple source languages per zip file."
	"company_id", "integer", "No", "Used when a user belongs to more than one client company."
	"deadline", "string", "No", "If provided, will set the project deadline to this date and time. Format ISO 8601 standard. For example: 2023-11-01T09:39:37Z or 2023-11-01T09:39:37+00:00 "
	"callback_url", "URL", "No", "If provided, LanguageDesk will send a POST request to this URL after project is created. The request will contain the project ID and status."
	"product_line_id", "integer", "No", "If provided, will set the project product line to this value."
	"document_type_id", "integer", "No", "If provided, will set the project document/text type to this value."

The following fields are not part of the request and will be generated automatically as follows in the project:

* Project name: [content identifier] [DD/MM/YYYY hh:mm] [source_language_code] [target_language_codes, comma separated] (API requests)
* Deadline: Same datetime as the request itself (rounded up to the next hour, as deadline needs to be in the future) or value provided in the request.
* All other project fields are left blank

Note: The project will be created in exactly the same way as a manually created project. The only difference is that no email notification will be send to the client contact person of the project.

.. csv-table:: Response
	:header: "Field", "Possible values", "Comment"
	:widths: 25, 20, 55

	"project.id", "string", "The project number of the project the request was added to. Can be used later to retrieve translated content."
	"project.status", "string", "Project status after new request being added into container"
	"status", "integer", "HTTP error code"
	"message_code", "* success
	* contact_person_invalid
	* source_language_invalid
	* source_languages_invalid
	* content_identifier_toolong
	* file_transfer_method_invalid
	* host_couldnotconnect
	* md5_check_failed", "Possible error messages when creating or adding into project"
	"message", "string", "Textual description of error to present to user"
	"company_id", "integer", "Project company ID"

Example requests
++++++++++++++++

**Scenario 1: Project details are submitted via HTTP API request, LanguageDesk then pull the actual files to translate from the provided FTP server (credentials are stored encrypted in the LanguageDesk database).**


.. code-block:: JSON

	{
		"project": {
			"contact_person": "user@client.com",
			"content_identifier": "Website",
			"source_language": "en",
			"target_languages": ["de-DE","en-GB"],
			"file_transfer_method": "ftp-pull",
			"host": "hotfolder.languagedesk.com",
			"company_id": "12345",
			"callback_url": "http://client.com/callback",
			"files": {
				"1": {
					"source_path": "/path1/",
					"target_path": "/path1_target/",
					"name": "file_english_name_1.doc",
					"md5_checksum": "e696b5aed3fca73e381d86a054706485"
				},
				"2": {
					"path": "/path2/",
					"name": "file_english_name_2.doc",
					"md5_checksum": "b7006a65aed3fca73e381d86a054706678"
				}
			}
		}
	}

or...

.. code-block:: JSON

	{ 
		"directories": {
			"remove_source_path": true,
			"source_path": ["/import", "/import2"],
			"target_path": "/export"
		},
		"mode": "container"
	}

**Scenario 2: Project details and files are submitted via HTTP API request.**

.. code-block:: JSON

	{
		"project": {
			"contact_person": "user@client.com",
			"content_identifier": "Website",
			"source_language": "en",
			"target_languages": ["de-DE", "en-GB"],
			"file_transfer_method": "http-push",
			"company_id": "12345",
			"callback_url": "http://client.com/callback",
			"files": {
				"1": {
					"name": "file_english_name_1.doc",
					"file": "<tmp/multipart uploaded file>"
				},
				"2": {
					"name": "file_english_name_2.doc",
					"file": "<tmp/multipart uploaded file>"
				}
			}
		}
	}

**Sample script (Python)**

.. code-block::

	import requests
	import base64

	file_content = base64.b64encode(b'Content for translation')

	files = {"1": {"name": "sample",
	        "file": str(file_content, encoding="UTF-8")
	        }}
	project_info = {"contact_person": "user@example.com",
	                "content_identifier": "Sample",
	                "source_language": "en",
	                "target_languages": ["de-de", "fr-fr"],
	                "file_transfer_method": "http-push",
	                "files": files
	                }
	json_data = {"project": project_info}

	url = "http://staging.languagedesk.com/api/v1/projects"
	params = {"auth_token": "MY_SECRET_TOKEN"}

	req = requests.Request('POST',
	                       url,
	                       params=params,
	                       json=json_data)
	prepared_req = req.prepare()
	session = reqests.Session()
	response = session.send(prepared_req)

	print(response.headers)
	print(response.content)

Example responses
+++++++++++++++++

**Example response 1 (OK)**

.. code-block:: JSON

	{
		"status": 200,
		"project": {
			"id": "23456",
			"company_id": "12345",
			"status": "in_progress"		
		}
	}

**Example response 2 (ERROR)**

.. code-block:: JSON

	{
		"status": 400,
		"messages": {
			"missing_target_languages": "missing target languages",
			"content_identifier": "Can't be blank",
			"files": "Can't be blank"
		}
	}

	