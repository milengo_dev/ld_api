Download invoices
==============================

GET https://app.languagedesk.com/api/v1/projects/:id/invoices

.. csv-table:: Request
	:header: "Field","Possible Values","Required?","Comment"
	:widths: 25, 25, 10, 40

	"id", "integer number", "yes", "The project number provided when the request was posted"

Example requests
++++++++++++++++

.. csv-table:: Response
	:header: "Field", "Possible Values", "Comment"
	:widths: 25, 20, 55

	"project.id", "integer", "Project number"

Example responses
+++++++++++++++++

**Response 1**

.. code-block:: JSON

  {
   "status": 200,
   "company_id": 1,
   "invoices_urls":[
     { "id": "567", "url": "http://…/…pdf" }
   ]
  }

**Response 2**

.. code-block:: JSON

	{
	 "status": 400,
	 "messages": {
	  "key": "error message"
	 }
	}