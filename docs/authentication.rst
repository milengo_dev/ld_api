Authentication
==============

Authentication for API call/requests is based on secure tokens. The token needs to be passed as a http parameter or header to authenticate requests. For example:

POST to https://app.languagedesk.com/api/v1/projects?auth_token="ABD123DFG323434"

Please contact us at support@languagedesk.com to apply API access and get your secure token.

Optionally, the API requests for a specific customer may be restricted to a specific IP (range).