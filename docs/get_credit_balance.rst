Credit Balance
===================

Credit Balance can be queries for any company that uses this feature.

GET /api/v1/projects/credit_balance


Example responses
+++++++++++++++++

**Response 1**

.. code-block:: JSON

	{
		"credit_balance": 1000
	}

**Response 2**

Status: 422 

.. code-block:: JSON

	{
		"messages": {
			"credits_disabled": "credits are corrently disabled for your company"
		}
	}
