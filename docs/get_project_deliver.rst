Project deliver
===================

GET /api/v1/projects/:id/deliver

.. csv-table:: Request
	:header: "Field","Possible Values","Required?","Comment"
	:widths: 25, 25, 10, 40

	"id", "integer number", "yes", "The project number provided when the request was posted"


Example responses
+++++++++++++++++

**Response 1**

.. code-block:: JSON

	{
		"status": 200,
		"project": {
			"id": "23456",
			"name": "Project name",
			"status": "in_progress"
		}
	}

**Response 2**

.. code-block:: JSON

	{
		"status": 404,
		"messages": [{
			"project_not_found": "project not found"
		}]
	}
