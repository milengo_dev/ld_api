Change project status
==============================

POST https://app.languagedesk.com/api/v1/projects/:id/change_status

.. csv-table:: Request
	:header: "Field","Possible Values","Required?","Comment"
	:widths: 25, 25, 10, 40

	"id", "integer number", "yes", "The project number provided when the request was posted"

Example requests
++++++++++++++++

.. csv-table:: Request
  :header: "Field","Possible Values","Required?","Comment"
  :widths: 25, 25, 10, 40

  "project.id", "integer number", "yes", "The project number provided when the request was posted"
  "project.status", "json", "yes", "The request body should be a JSON object with the following fields"
  "callback_url", "string", "no", "This parameter will be used to make a POST request on callback_url with content of project id and state"

.. code-block:: JSON

  {
    "project": {
      "id": "123",
      "status": "STATUS",
    },
    "callback_url": "http://…"
  }

STATUS can be: "draft", "potential", "cancel", "decline" or "approve_quote"

Example Response
++++++++++++++++

.. csv-table:: Response
  :header: "Field", "Possible Values", "Comment"
  :widths: 25, 20, 55

  "project.id", "integer", "Project number"
  "project.name", "string", "Project name"
  "project.status", "string", "Project status"

.. code-block:: JSON

  {
    "project": {
      "id": "123",
      "name": "Project name",
      "status": "potential"
    }
  }

Example error response
++++++++++++++++++++++

.. code-block:: JSON

  {
    "status": 422,
    "messages": [
      {
        "key": "error message"
      }
    ]
  }