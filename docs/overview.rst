API Usage Steps
===============
1. Client should obtain authentication token for LanguageDesk
2. Client should use REST compatible library and implement client side software accordingly, following API description
3. Client side software should configured using provided authentication token

To obtain authentication token, please contact sales@milengo.com

Note:
+++++
* Production base URL: https://app.languagedesk.com
* Staging base URL: https://staging.languagedesk.com