Get project
===================

GET https://app.languagedesk.com/api/v1/projects/23456

.. csv-table:: Request
	:header: "Field","Possible Values","Required?","Comment"
	:widths: 25, 25, 10, 40

	"id", "integer number", "yes", "The project number provided when the request was posted"

Example requests
++++++++++++++++

GET https://app.languagedesk.com/api/v1/projects/23456

.. csv-table:: Response
	:header: "Field", "Possible Values", "Comment"
	:widths: 25, 20, 55

	"project.id", "integer", "Project number"
	"project.delivery_date", "* datetime
	* not_available", "if delivery_date equals request date, it has not yet been updated by the PM, so the value not_available is returned"
	"project.status", "* in_progress
	* completed
	* cancelled
	* draft
	* quote_request"
	"status", "integer", "HTPP Error code"
	"messages", "key/value pairs, error code->error message", "Error code and textual description of error to present to user."
	"company_id", "integer", "Project company ID"


Example responses
+++++++++++++++++

**Response 1**

.. code-block:: JSON

	{
		"status": 200,
		"project": {
			"id": "23456",
			"company_id": "12345",
			"name": "Website 10/05/2014 12:00 pt-PT:en-EN,de-DE (API requests)",
			"delivery_date": "2014-01-01T00:00",
			"status": "in_progress"
		}
	}

**Response 2**

.. code-block:: JSON

	{
		"status": 404,
		"messages": {
			"project_not_found": "project not found"
		}
	}
