Download delivery files
==============================

GET https://app.languagedesk.com/api/v1/projects/:id/download_delivery_files

.. csv-table:: Request
	:header: "Field","Possible Values","Required?","Comment"
	:widths: 25, 25, 10, 40

	"id", "integer number", "yes", "The project number provided when the request was posted"

Example requests
++++++++++++++++

.. csv-table:: Response
	:header: "Field", "Possible Values", "Comment"
	:widths: 25, 20, 55

	"project.id", "integer", "Project number"
	"project.delivery_files_url", "URL with https://files.languagedesk.com/...", "URL that can be used to download the zip file containing all project delivery files."
	"project.status", "* in_progress
	* completed
	* canceled
	* draft
	* quote_request"
	"status", "integer", "HTTP Error code"
	"message", "key/value pairs: { error_code=>error_message}", "Error code and textual description of error to present to user"
	"company_id", "integer", "Project company ID"

Example responses
+++++++++++++++++

**Response 1**

.. code-block:: JSON

	{
		"status": 200,
		"project": {
			"id": "23456",
			"delivery_files_url": "http://…/…zip",
			"status": "completed"
		}
	}

**Response 2**

.. code-block:: JSON

	{
		"status": 404,
		"messages": {
			"project_not_found": "project not found"
		}
	}